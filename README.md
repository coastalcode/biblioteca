# Biblioteca

A simple EBook management system written in Java with a Bootstrap 4 interface. Project goals are for basic functionality
to be available without JavaScript and to use lightweight libraries wherever possible. Project name refers to the
Biblioteca Rap from the TV show *Community*. Sadly I don't actually speak any Spanish.


**Current Status:** Current code generates an index from a directory of .epub files. Code & Issue submissions welcome but
non-technical users may want to wait a few days.


![Screenshot](/screenshot.jpg "Current Screenshot")

## Installation
Clone and run 'gradle make' to build. Dependencies should be downloaded automatically.

Run the class 'com.coastalcode.biblioteca.Index' and browse to 'http://localhost:4567/'.

Sorry for the very bare bones instrictions,I hope to update the build process at some point.



