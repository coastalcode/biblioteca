package com.coastalcode.biblioteca;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import spark.Request;
import spark.servlet.SparkApplication;

import java.io.StringWriter;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

/**
 * Convenient base class that provides methods for Velocity rendering, Pagination and setting/saving basic application settings.
 */
public abstract class SparkVelocityApplication implements SparkApplication {


    /**
     * Renders the response using Velocity templates. Attributes are added to the context as simple name/value pairs.
     * @param template The name of the template file to use for rendering (.vm is automatically added).
     * @param state A SiteState object which is passed to the template as variable $state.
     * @param request The request containing necessary variables for the template.
     * @return Returns a rendered template as a String ready to return to browser.
     */
    public static String renderTemplate(String template, Messages state, Request request) {
        //Initialize Velocity
        VelocityEngine ve = new VelocityEngine();
        Properties p = new Properties();
        p.setProperty("resource.loader", "class");  //classpath resource loader since we're inside war
        p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        ve.init(p);

        //Create a velocity context and add page parameters to it for rendering
        VelocityContext context = new VelocityContext();
        Set<String> attrList = request.attributes();
        Iterator<String> iter = attrList.iterator();
        String attrCurrent;
        while (iter.hasNext()) {
            attrCurrent = iter.next();
            context.put(attrCurrent, request.attribute(attrCurrent));
        }

        //Pass in and clear the messages/errors
        if(state.isMessages()){
            context.put("success_messages", state.getSuccessMessages());
            context.put("info_messages", state.getInfoMessages());
        }
        if(state.isError()){
            context.put("error_messages", state.getErrorMessages());
        }
        if(!state.deferRender()){ //clear messages except if there is a redirect
            state.clearMessages();
            request.session().attribute("state", state);
        }

        //Pass in the standard Velocity Tools
        /*
        context.put("esc", new org.apache.velocity.tools.generic.EscapeTool());
        context.put("number", new org.apache.velocity.tools.generic.NumberTool());
        context.put("math", new org.apache.velocity.tools.generic.MathTool());
        context.put("string_utils", new org.apache.velocity.util.StringUtils());
        context.put("date", new org.apache.velocity.tools.generic.DateTool());
        context.put("state", state);
        */
        //Load the template for rendering.
        Template veTemplate = null;
        try
        {
            veTemplate = ve.getTemplate(/*state.getTheme() + */"/" + template + ".vm");
            //System.out.println("TMN://Velocity Template: " + veTemplate);
        }
        catch( ResourceNotFoundException rnfe )
        { System.out.println("Velocity Template '" + template + ".vm" + "' not found.");}
        catch( ParseErrorException pee )
        { System.out.println("Parse Exception in Velocity Template '" + template + ".vm" + "'");}
        catch( MethodInvocationException mie )
        { System.out.println("Invocation Exception in Velocity Template '" + template +".vm" + "'");}
        catch( Exception e )
        { System.out.println("General Exception in Velocity Template '" + template + ".vm" + "'");}

        //combine the template with the variables (context) and render
        StringWriter sw = new StringWriter();
        veTemplate.merge( context, sw );
        return  sw.toString();
    }


    /*
    //Send an e-mail notification to specified users.
    HashMap<String, String> mailServerProperties = null;
    public void sendNotification(String notificationIdentifier, HashMap<String, String> templateVariables, Session session, SiteState state){
        if(mailServerProperties == null){
            mailServerProperties = new HashMap<>();
            Criteria criteria = session.createCriteria(SettingsEntity.class);
            criteria.add(Restrictions.eq("category", "mail_properties"));
            List<SettingsEntity> mailSettings = (List<SettingsEntity>)criteria.list();
            for(SettingsEntity setting : mailSettings){
                mailServerProperties.put(setting.getId(), setting.getValue());
            }
        }
        if(notificationIdentifier.equalsIgnoreCase("test")){ //Send Test E-Mail to Check Server Settings
            System.out.println("Sending test message to " + templateVariables.get("test_address") + ".");
            HtmlEmail message = prepareMailMessage(templateVariables.get("test_address"), templateVariables.get("test_address"), "NSFire.ca Test E-Mail", "test", templateVariables);
            try {
                message.send();
            } catch (EmailException e) {
                e.printStackTrace();
                state.addErrorMessage(e.toString());
                state.setDeferRender(true);
            }
        } else{
            List<String> addresses = getAddressList("notify_all", session);
            if(addresses.isEmpty()){
                System.out.println("Notification list is empty. No messages will be sent.");}
            for(String address : addresses){
                // Create & Send Messages
                HtmlEmail message = prepareMailMessage(address, address, "NSFire.ca Update", notificationIdentifier, templateVariables);
                try {
                    message.send();
                } catch (EmailException e) {
                    e.printStackTrace();
                    state.addErrorMessage(e.toString());
                    state.setDeferRender(true);
                }
            }
        }
    }
    private HtmlEmail prepareMailMessage(String recipientAddress, String recipientName, String subject, String template, HashMap<String, String> templateVariables) {
        // Create the email message - use properties file values
        HtmlEmail email = new HtmlEmail();
        email.setHostName(mailServerProperties.get("mail_host"));
        email.setSmtpPort(Integer.parseInt(mailServerProperties.get("mail_port")));
        email.setAuthenticator(new DefaultAuthenticator(mailServerProperties.get("mail_user"), mailServerProperties.get("mail_pass")));
        email.setStartTLSEnabled(Boolean.parseBoolean(mailServerProperties.get("mail_tls")));
        try {
            email.addTo(recipientAddress, recipientName);
            email.setFrom(mailServerProperties.get("mail_from"), mailServerProperties.get("mail_title"));
        } catch (EmailException e) {
            e.printStackTrace();
        }
        // embed the image and get the content id
        URL url = null;
        try {
            url = new URL("http://www.nsfire.ca/theme/portal/images2/email_logo.jpeg");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String cid = null;
        try {
            email.setSubject(subject);
            cid = email.embed(url, "NSFire.ca Logo");
            email.setHtmlMsg(renderEMailTemplate(template + "_html", templateVariables));
            email.setTextMsg(renderEMailTemplate(template + "_text", templateVariables));
        } catch (EmailException e) {
            e.printStackTrace();
        }
        return email;
    }
    public void saveAddressList(String notificationId, List<String> addresses, Session session){
        session.beginTransaction();
        SettingsEntity setting = (SettingsEntity)session.get(SettingsEntity.class, notificationId);
        if(setting == null){ //if the setting does not exist persist a new setting with the specified name/id.
            System.out.println("Setting not found. Generating new setting entry with name: " + notificationId);
            setting = new SettingsEntity();
            setting.setId(notificationId);
        }
        //Modify entity here.
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(addresses);
        setting.setValue(json);
        session.save(setting); //Save updated object.
        session.getTransaction().commit();
    }
    public List<String> getAddressList(String notificationId, Session session){
        SettingsEntity setting = (SettingsEntity)session.get(SettingsEntity.class, notificationId);
        if(setting == null){ //if the setting does not exist persist a new setting with the specified name/id.
            System.out.println("Setting not found. Returning empty List. " + notificationId);
            return Lists.newArrayList();
        }
        //Modify entity here.
        Gson gson = new GsonBuilder().create();
        List<String> addresses = gson.fromJson(setting.getValue(), List.class);
        return addresses;
    }


    //Basic key/value storage for simple app settings. Could use Postgres HStore instead?
    public void saveSettings(String name, List elements, Session session){
        SettingsEntity setting = (SettingsEntity)session.get(SettingsEntity.class, name);
        if(setting == null){ //if the setting does not exist persist a new setting with the specified name/id.
            System.out.println("Setting not found. Generating new setting entry with name: " + name);
            setting = new SettingsEntity();
            setting.setId(name);
        }
        //Modify entity here.
        session.save(setting); //Save updated object.
    }
    public List<String> getStringSettings(String name, Session session){
        return Lists.newLinkedList();
    }
    public List<Integer> getIntegerSettings(String name, Session session){
        return Lists.newLinkedList();
    }

    //TODO:// Update Pagination so it takes a criteria object? Easier to do custom entities like sub galleries than assuming a simple query.
    public PaginationState generatePaginationState(Request request, Session session, Integer siteid, Class clazz, int resultsPerPage) {
        return generatePaginationState(request, session, siteid, null, clazz, resultsPerPage);
    }
    public PaginationState generatePaginationState(Request request, Session session, Integer siteid, Integer subid, Class clazz, int resultsPerPage) {
        //pagination parameter to set current page.
        String page_raw = request.queryParams("page");
        int page_parsed = 1;
        if (page_raw == null || page_raw.isEmpty()) {
            page_parsed = 1; //default to first oage
        }else{
            try{page_parsed = Integer.parseInt(page_raw);}
            catch(Exception e){ page_parsed = 1; }
        }

        Criteria criteria = null;
        criteria = session.createCriteria(clazz);
        criteria.add(Restrictions.eq("visible", true)); //ignore deleted items
        if(siteid != 1){criteria.add(Restrictions.eq("siteId", siteid));}
        if(subid != null){criteria.add(Restrictions.eq("galleryId", subid));}
        //Generate Pagination variables. Use scrollable result to get total number of results.
        ScrollableResults paginationResults = criteria.scroll();
        paginationResults.last();
        int total = paginationResults.getRowNumber() + 1;
        PaginationState pagination = new PaginationState(page_parsed, (int) Math.ceil((double) total / resultsPerPage), resultsPerPage);
        request.attribute("pagination", pagination);
        paginationResults.close();
        return pagination;
    }
    public PaginationState generatePaginationState(Request request, Session session, Integer siteid, Criteria criteria, int resultsPerPage) {
        //pagination parameter to set current page.
        String page_raw = request.queryParams("page");
        int page_parsed = 1;
        if (page_raw == null || page_raw.isEmpty()) {
            page_parsed = 1; //default to first oage
        }else{
            try{page_parsed = Integer.parseInt(page_raw);}
            catch(Exception e){ page_parsed = 1; }
        }

        //Generate Pagination variables. Use scrollable result to get total number of results.
        ScrollableResults paginationResults = criteria.scroll();
        paginationResults.last();
        int total = paginationResults.getRowNumber() + 1;
        PaginationState pagination = new PaginationState(page_parsed, (int) Math.ceil((double) total / resultsPerPage), resultsPerPage);
        request.attribute("pagination", pagination);
        paginationResults.close();
        return pagination;
    }
    */

    /*
    public static String renderEMailTemplate(String template, HashMap<String, String> templateParams) {
        //Initialize Velocity
        VelocityEngine ve = new VelocityEngine();
        Properties p = new Properties();
        p.setProperty("resource.loader", "class");  //classpath resource loader since we're inside war
        p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        ve.init(p);

        //Create a velocity context and add page parameters to it for rendering
        VelocityContext context = new VelocityContext();

        for (Map.Entry entry : templateParams.entrySet()) { //add parameters to template context
            String key = (String)entry.getKey();
            String value = (String)entry.getValue();
            context.put(key, value);
        }

        //Pass in the standard Velocity Tools
        context.put("esc", new org.apache.velocity.tools.generic.EscapeTool());
        context.put("number", new org.apache.velocity.tools.generic.NumberTool());
        context.put("math", new org.apache.velocity.tools.generic.MathTool());
        context.put("string_utils", new org.apache.velocity.util.StringUtils());
        context.put("date", new org.apache.velocity.tools.generic.DateTool());

        //Load the template for rendering.
        Template veTemplate = null;
        try
        {
            veTemplate = ve.getTemplate("admin/email_templates/" + template + ".vm");
            //System.out.println("TMN://Velocity Template: " + veTemplate);
        }
        catch( ResourceNotFoundException rnfe )
        { System.out.println("Velocity Template '" + template + ".vm" + "' not found.");}
        catch( ParseErrorException pee )
        { System.out.println("Parse Exception in Velocity Template '" + template + ".vm" + "'");}
        catch( MethodInvocationException mie )
        { System.out.println("Invocation Exception in Velocity Template '" + template +".vm" + "'");}
        catch( Exception e )
        { System.out.println("General Exception in Velocity Template '" + template + ".vm" + "'");}

        //combine the template with the variables (context) and render
        StringWriter sw = new StringWriter();
        veTemplate.merge( context, sw );
        return  sw.toString();
    }
    */
}