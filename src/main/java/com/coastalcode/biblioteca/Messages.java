package com.coastalcode.biblioteca;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Troy MacNeil
 */
public class Messages {

    //Error and message handling
    Boolean messages = false;
    Boolean error = false;
    Boolean deferRender = false; //render now if false, wait until next request if true
    ArrayList<String> error_messages;
    ArrayList<String> success_messages;
    ArrayList<String> info_messages;

    public Boolean isMessages(){
        return messages;
    }
    public Boolean isError(){
        return error;
    }
    public Boolean deferRender(){
        return deferRender;
    }

    public ArrayList<String> getErrorMessages(){
        if(error_messages != null) return error_messages;
        else return new ArrayList<String>();
    }
    public ArrayList<String> getSuccessMessages(){
        if(success_messages != null) return success_messages;
        else return new ArrayList<String>();
    }
    public ArrayList<String> getInfoMessages(){
        if(info_messages != null) return info_messages;
        else return new ArrayList<String>();
    }

    public void addErrorMessage(String error){
        if(error_messages == null) error_messages = new ArrayList<String>();
        error_messages.add(error);
        this.error = true;
    }
    public void addSuccessMessage(String success){
        if(success_messages == null) success_messages = new ArrayList<String>();
        success_messages.add(success);
        this.messages = true;
    }
    public void addInfoMessage(String info){
        if(info_messages == null) info_messages = new ArrayList<String>();
        info_messages.add(info);
        this.messages = true;
    }

    public void clearMessages(){
        this.error_messages = null;
        this.success_messages = null;
        this.info_messages = null;
        this.messages = false;
        this.error = false;
    }
}