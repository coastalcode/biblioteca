package com.coastalcode.biblioteca.DAO;

import lombok.Data;

import java.util.List;

/**
 * Created by troy on 2018-02-17.
 */
@Data
public class BookDAO {
    int bookId;
    String title;
    String authorFirstName;
    String authorLastName;
    String authorName;
    List authors;
    List publishers;
    String filename;
    String filepath;
    int library;
    String libraryPath;
    String coverFormat;
    String coverFilename;
    String coverFilepath;
}
