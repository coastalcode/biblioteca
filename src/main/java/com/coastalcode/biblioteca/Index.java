package com.coastalcode.biblioteca;

import com.coastalcode.biblioteca.DAO.BookDAO;
import com.coastalcode.biblioteca.util.Filters;
import nl.siegmann.epublib.domain.*;
import nl.siegmann.epublib.epub.EpubReader;
import spark.ModelAndView;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static spark.Spark.*;
import static spark.debug.DebugScreen.enableDebugScreen;

public class Index {

    public static String themePath = "/templates/";

    public static void main(String[] args) {
        port(4567);
        staticFiles.location("/public");
        staticFiles.externalLocation("/Users/troy/Projects/biblioteca/");
        //staticFiles.expireTime(600L); //disable file cache for development purposes.
        enableDebugScreen();

        //Set filters for all requests
        before("*", Filters.addTrailingSlashes);



        get("/", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("list", true);
            model.put("books", getAllBooks());
            return render(model, themePath + "index.vm");
        });

        get("/books", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("list", true);
            model.put("books", getAllBooks());
            return render(model, themePath + "index.vm");
        });

        get("/books/:bookid/", (request, response) -> {    //TODO:Clearly need to validate input...
            Map<String, Object> model = new HashMap<>();
            model.put("detail", true);
            model.put("book", getBook(Integer.parseInt(request.params("bookid"))));
            return render(model, themePath + "index.vm");
            //return "Hello: " + request.params(":name");
        });

        get("/scanlib/:libid/*", (req, res) -> { //functions to initiate library updates..Obviously needs a rewrite!
            Map<String, Object> model = new HashMap<>();

            Boolean full = false; //do full update?
            int libId = Integer.parseInt(req.params("libid"));  //get library id
            if(req.splat().length > 0){ //check for additional command
                if((req.splat()[0]).equals("full")){
                    full = true;
                    System.out.println("Full Rebuild? " + full);
                }
            }
            Boolean ajax = Boolean.parseBoolean(req.queryParams("ajax")); //is this an ajax request?
            System.out.println("AJAX: " + ajax);

            //basic update & return HTML list...
            ArrayList<BookDAO> books = scanLibraryDirectory(libId, true);
            //TODO:Add message to return results
            model.put("books", getAllBooks());
            if(!ajax){
                res.redirect("/");  //try redirect to main index, not an ajax request
            }
            //assume ajax request and return partial html list render
            return render(model, themePath + "book_list.vm");
        });
    }

    private static ArrayList<BookDAO> scanLibraryDirectory(int libraryId, Boolean doFullRebuild) {
        //TODO: Use check for library id instead of hardcoded paths
        String libraryDir = "books_lib";
        String coverDir = "covers";


        if(!doFullRebuild){
            //TODO: add code for partial scan of only new books
        }

        //Get list of book filenames...
        File library = new File(libraryDir);
        library.mkdir(); //create directory if it doesn't exist
        File[] libraryFilenames = library.listFiles((File dir, String name) -> {
            return name.toLowerCase().endsWith(".epub"); //only check .epub files for now, should note files that don't match?
        });

        ArrayList<BookDAO> books = new ArrayList<>(libraryFilenames.length);

        //Temporarily create directory for covers
        File coversDir = new File(coverDir);
        coversDir.mkdir(); //create directory if it doesn't exist

        //Parse each book and add to model
        EpubReader epubReader = new EpubReader();
        for (int i=0; i<libraryFilenames.length; i++) {
            try {
                Book rawBook = epubReader.readEpub(new FileInputStream(libraryFilenames[i]));
                BookDAO book = new BookDAO();
                book.setFilename(libraryFilenames[i].getName());
                book.setFilepath(libraryFilenames[i].getParent());
                Metadata metadata;
                String title = ""; List publishers = new ArrayList<>(); List authors = new ArrayList(0); String first = ""; String second = "";
                try{
                    metadata = checkNotNull(rawBook.getMetadata());
                    title = checkNotNull(metadata.getFirstTitle());
                    authors = checkNotNull(metadata.getAuthors());
                    Author author = (Author)checkNotNull(authors.get(0));
                    book.setAuthorFirstName(checkNotNull(author.getFirstname()));
                    book.setAuthorLastName(checkNotNull(author.getLastname()));
                    publishers = checkNotNull(metadata.getPublishers());
                    book.setPublishers(publishers);
                }
                catch(Exception e){System.out.println(libraryFilenames[i].toString());}
                book.setTitle(title);
                book.setAuthors(authors);

                String coverFormat = "";
                InputStream coverStream;
                File file = new File(coversDir, i + ".jpg");
                book.setCoverFilepath(coversDir.toString());
                try{
                    Resource cover = checkNotNull(rawBook.getCoverImage(), "No Cover Image.");
                    MediaType mediaType = checkNotNull(cover.getMediaType(), "No Media Type");
                    coverFormat = checkNotNull(mediaType.getName(), "No Media Type Name");
                    coverStream = checkNotNull(cover.getInputStream(), "No Input Stream");
                    Files.copy(checkNotNull(coverStream, "No In Stream to Copy"), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    book.setCoverFilename(i + ".jpg"); //place here so if there is no image filename won't be entered into db
                }catch(Exception e){System.out.println(libraryFilenames[i].toString());}
                book.setCoverFormat(coverFormat);
                books.add(book); //add for return to caller.
                addNewBook(book);
                rawBook = null; metadata = null;
            }catch(Exception e){
                System.out.println(e.toString());
            }
        }
        return books;
    }

    private static BookDAO getBook(int bookid) {
        //add book to global db, assume default location biblioteca.db for now
        BookDAO book = new BookDAO();
        Connection connection = null;
        try {
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:biblioteca.db");

            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("select book_id, title, author_firstname, author_lastname, cover_filename, filepath, filename from books where book_id=" + bookid);

            while(results.next()) {
                book.setBookId(results.getInt("book_id"));
                book.setTitle(results.getString("title"));
                book.setAuthorFirstName(results.getString("author_firstname"));
                book.setAuthorLastName(results.getString("author_lastname"));
                book.setCoverFilename(results.getString("cover_filename"));
                book.setFilepath(results.getString("filepath"));
                book.setFilename(results.getString("filename"));
            }
        }
        catch(SQLException e) {
            // if the error message is "out of memory", it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally {
            try {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e) {
                // connection close failed.
                System.err.println(e);
            }
        }
        return book;
    }

    private static ArrayList<BookDAO> getAllBooks() {
        //add book to global db, assume default location biblioteca.db for now
        ArrayList<BookDAO> books = new ArrayList<>(30);
        Connection connection = null;
        try {
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:biblioteca.db");

            Statement statement = connection.createStatement();
            ResultSet results = statement.executeQuery("select book_id, title, author_firstname, author_lastname, cover_filename from books where book_id>0");

            //Create list of books from query
            while(results.next())
            {
                BookDAO book = new BookDAO();
                book.setBookId(results.getInt("book_id"));
                book.setTitle(results.getString("title"));
                book.setAuthorFirstName(results.getString("author_firstname"));
                book.setAuthorLastName(results.getString("author_lastname"));
                book.setCoverFilename(results.getString("cover_filename"));
                books.add(book);
            }
        }
        catch(SQLException e) {
            // if the error message is "out of memory", it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally {
            try {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e) {
                // connection close failed.
                System.err.println(e);
            }
        }
        return books;
    }

    private static Boolean addNewBook(BookDAO book) {
        //TODO:Fix image numbering issue by adding autoincrement field
        //select seq from sqlite_sequence where name="table_name"
        //www.sqlite.org/autoinc.html

        //add book to global db, assume default location biblioteca.db for now
        Connection connection = null;
        try {
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:biblioteca.db");

            //add the DAO to the database, should check if exists first?
            String columns = "title, author_firstname, author_lastname, filename, filepath, cover_format, cover_filename, cover_filepath, authors";
            PreparedStatement statement = connection.prepareStatement("insert into books (" + columns + ") values (?,?,?,?,?,?,?,?,?)");
            statement.setQueryTimeout(10);  // set timeout to 10 sec.
            statement.setString(1, book.getTitle());
            statement.setString(2,book.getAuthorFirstName());
            statement.setString(3,book.getAuthorLastName());
            statement.setString(4,book.getFilename());
            statement.setString(5,book.getFilepath());
            statement.setString(6,book.getCoverFormat());
            statement.setString(7,book.getCoverFilename());
            statement.setString(8,book.getCoverFilepath());
            statement.setString(9,book.getAuthors().toString());
            statement.executeUpdate();
        }
        catch(SQLException e) {
            // if the error message is "out of memory", it probably means no database file is found
            System.err.println(e.getMessage());
        }
        finally {
            try {
                if(connection != null)
                    connection.close();
            }
            catch(SQLException e) {
                // connection close failed.
                System.err.println(e);
            }
        }
        return true;
    }

    public static String render(Map<String, Object> model, String templatePath) {
        return new VelocityTemplateEngine().render(new ModelAndView(model, templatePath));
    }
}