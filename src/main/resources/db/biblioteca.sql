
CREATE TABLE "books" (
"book_id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ON CONFLICT ROLLBACK,
"title" TEXT,
"author_firstname" TEXT,
"author_lastname" TEXT,
"authors" TEXT,
"filename" TEXT,
"filesize" TEXT,
"filepath_relative" TEXT,
"filepath_canonical" TEXT,
"library" INTEGER,
"cover_format" TEXT,
"cover_filename" TEXT NOT NULL,
"cover_filepath_relative" TEXT,
"cover_filepath_canonical" TEXT
);


CREATE TABLE "libraries" (
"library_id" INTEGER PRIMARY KEY NOT NULL ON CONFLICT ROLLBACK,
"library_name" TEXT NOT NULL ON CONFLICT ROLLBACK,
"library_count" INTEGER,
"library_dir" TEXT,
"library_path_canonical" TEXT,
"library_path_relative" TEXT
);

CREATE TABLE "tags" (
"tag_id" INTEGER PRIMARY KEY NOT NULL ON CONFLICT ROLLBACK,
"tag_name" TEXT NOT NULL ON CONFLICT ROLLBACK
);

